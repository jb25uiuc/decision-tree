module InputParser where

import DecisionTree

import Data.Attoparsec.Text

trainLineParser :: Parser (Int, [(Int, Int)])
trainLineParser = do
    cat <- classParser
    space
    attrs <- attrParser `sepBy` (char ' ')
    return (cat, attrs)

testLineParser :: Parser [(Int, Int)]
testLineParser = attrParser `sepBy` (char ' ')

classParser :: Parser Int
classParser = decimal

attrParser :: Parser (Int,Int)
attrParser = do
    a <- decimal
    skip (== ':')
    v <- decimal
    return (a,v)

trainDataParser :: Parser (Dataset Int (Int, Int))
trainDataParser = trainLineParser `sepBy` endOfLine

testDataParser :: Parser [[(Int, Int)]]
testDataParser = testLineParser `sepBy` endOfLine