module Main where

import DecisionTree
import InputParser

import System.Environment
import Data.Attoparsec.Text (parseOnly)
import qualified Data.Text as T
import qualified Data.Text.IO as T

main :: IO ()
main = do
    args <- getArgs
    let threshold = read $ args !! 0 :: Double
    trainingText <- T.readFile $ args !! 1
    testingText <- T.readFile $ args !! 2
    let training = parseOnly trainDataParser trainingText
    let testing = parseOnly testDataParser testingText
    case (training, testing) of
        (Right trainData, Right testData) -> do
            let dt = train (giniSplitter threshold) trainData
            T.putStr $ T.unlines $ map (T.pack . show . (apply dt)) testData
        otherwise -> putStrLn "Something went wrong!"
